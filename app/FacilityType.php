<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityType extends Model
{
    protected $guarded =[];

    public function facility()
    {
        return $this->hasMany('App\Facility', 'type_id');
    }
    
    public function coupon_facility_type()
    {
        return $this->hasMany('App\CouponFacilityType');
    }
}
