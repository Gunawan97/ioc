<?php

namespace App\Http\Middleware;

use Closure;

class AuthToCore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->token==env("API_TOKEN","")){
            return $next($request);            
        } 
        else{
            return response()->json("Error!Not Authorized", 401);   
        }
        
    }
}
