<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bundle;
use App\BundleDetail;
use App\Facility;
use App\DetailFacility;
use App\Addon;
use App\Gallery;
use App\DetailOrder;
class BundleController extends Controller
{
    public function index(Request $request)
    {
        $bundles = Bundle::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $bundles  = $bundles->where($key, 'like', '%'.$value.'%');
                }
            }
        }
        $bundles=$bundles->orderBy('id','DESC')->paginate(20);
        
        return view('bundle.index')
        ->with('filter', $filter)
        ->with('bundles', $bundles);
    }
    public function create(){
        $bundle = new Bundle;
        $array=[];
        $facs= Facility::all()->pluck("id", "name");
        $add = Addon::all()->pluck("id", "name");
        foreach ($facs as $key=>$val) {
            $array = array_add($array, "F-".$val, $key);
        }
        foreach ($add as $key=>$val) {
            $array = array_add($array, "A-".$val, $key);
        }
        
        return view('bundle._form')
        ->with('facs', $array)
        ->with('bundle', $bundle);
    }
    public function store(Request $request)
    {
        // return $request;
        $bundle = new Bundle;
        $bundle->per_person = 0;     
        $bundle->frontend = 0;                                   
        $bundle->fill($request->except('detail','images'));
        return $bundle;
        if($request->per_person=="on"){
            $bundle->per_person = 1;
        }
        if($request->frontend=="on"){
            $bundle->frontend = 1;
        }
        $bundle->slug = str_slug($bundle->name, '_');        
        $bundle->save();
        if(count($request->detail)>0){
            foreach($request->detail as $key=>$det){
                if($key==0){
                    continue;
                }
                $dets = new BundleDetail;
                $str=explode("-",$det);     
                $dets->bundl_type = "App\Addons";
                if($str[0]=="F"){
                    $dets->bundl_type = "App\Facility";
                }
                $dets->bundl_id=$str[1];
                $dets->bundle_id=$bundle->id;
                $dets->save();
            }
        }

        $file = $request->file('images');
        if(count($file)){
            foreach($file as $f){
            $filename = $f->getClientOriginalName();
            $path = public_path().'/image/';
            $f->move($path, $filename);
            
            $gallery = new Gallery;
            $gallery->image_path = $filename;
            $gallery->galleriable_id = $bundle->id;
            $gallery->galleriable_type = 'Bundle';
            $gallery->save();
            $gallery->slug = str_slug($gallery->id.'_'.$gallery->title, '_');
            $gallery->save();
            }
        }

        
        $request->session()->flash('toast', 'Paket berhasil ditambahkan!');
        return redirect()->route('bundles.index');
    }
    public function edit($id)
    {
        $bundle = Bundle::find($id);
        $array=[];
        $fac = Facility::all()->pluck("id", "name");
        $add = Addon::pluck("id", "name");
        foreach ($fac as $key=>$val) {
            $array = array_add($array, "F-".$val, $key);
        }
        foreach ($add as $key=>$val) {
            $array = array_add($array, "A-".$val, $key);
        }

        
        // return $bundle->bundle_detail->pluck("id_helper_label");
        return view('bundle._form')
        ->with('facs', $array)
        ->with('bundle', $bundle);
    }
    public function update($id, Request $request)
    {
        $bundle = Bundle::find($id);
        $bundle->fill($request->except('detail','ids'));
        $bundle->frontend = 0;  
        $bundle->per_person = 0;
        
        if($request->per_person){
            $bundle->per_person = 1;
        }                    
        if($request->frontend){
            $bundle->frontend = 1;
        }
        $bundle->save();
        $detbun = BundleDetail::where("bundle_id",$bundle->id)->delete();
        if(count($request->detail)>0){
            foreach($request->detail as $key => $det){
                if($key==0){
                    continue;
                }
                $dets = new BundleDetail;
                $str=explode("-",$det);     
                $dets->bundl_type = "App\Addons";
                if($str[0]=="F"){
                    $dets->bundl_type = "App\Facility";
                }
                
                $dets->bundl_id=$str[1];
                $dets->bundle_id=$bundle->id;
                $dets->save();
            }
        }


        $request->session()->flash('toast', 'Paket  berhasil diubah!');
        return redirect()->route('bundles.index');
    }
    public function show(Bundle $bundle)
    {
        
        $gallery = Gallery::query();
        $gallery = $gallery->where('galleriable_type', '=', 'Bundle');
        $gallery = $gallery->where('galleriable_id', '=', $bundle->id);
        $gallery = $gallery->paginate(5, ['*'], 'gallery');

        return view('bundle.show', compact('bundle', 'gallery'));
    }
    public function destroy(Request $request, Bundle $bundle)
    {
        try {
                $chk = DetailOrder::where("item_id",$bundle->id)->where("item_type","LIKE","%Bundle")->count();        
                if($chk>0){
                    $request->session()->flash('error', 'Paket gagal dihapus. Bundle Telah dipakai dalam order');
                    return redirect()->route('bundles.index');
                                
                }
                $detbun = BundleDetail::where("bundle_id",$bundle->id)->delete();        
                $bundle->delete();
                $request->session()->flash('toast', 'Paket berhasil dihapus!');
                $bundle->delete();
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('error', 'Paket gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
    
        return redirect()->route('bundles.index');
    }

    public function getforbundle(Request $req)
    {
        $array=[];
        if($req->q="") {
            return [];  
        }
        $facs= Facility::where("name", "LIKE", "%".$req["q"]."%")->get()->pluck("id", "name");
        $add = Addon::where("name", "LIKE", "%".$req["q"]."%")->limit(10)->pluck("id", "name");
        foreach ($facs as $key=>$val) {
            $array = array_add($array, "F-".$val, $key);
        }
        foreach ($add as $key=>$val) {
            $array = array_add($array, "A-".$val, $key);
        }
        return $array;
    }

}
