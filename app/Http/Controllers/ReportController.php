<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use App\CustomerType;
use Carbon\carbon;
use App\Facility;
use App\DetailFacility;
use App\FacilityType;
use App\DetailOrder;
use Barryvdh\Snappy\Facades\SnappyPdf;

class ReportController extends Controller{
    // public function customer(Request $request){
    //     $orders = Order::query();
    //     $filter = [];
    //     if(isset($request->filter)) {
    //         $filter = $request->filter;
    //         foreach ($filter as $key => $value) {
    //             if(!empty($value)) {
    //                 $orders  = $orders->where($key, 'like', '%'.$value.'%');
    //             }
    //         }
    //     }
    //     $orders=$orders->orderBy('id','ASC');
    //     return view('report.index')->with('orders',$orders)->with('filter',$filter);
    // }
    public function index()
    {
        return view('report.index');
    }

    public function customer(Request $request){
        // return $request;
  
        // $notin = Order::whereDate("date","<",$request->filter['start'])->orwhereDate("date",">",$request->filter['end']);
        $cstyp = CustomerType::all();
        $result=[];
        foreach($cstyp as $cust){
            $results = Order::whereIn("customer_id",$cust->customer->pluck("id"));
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end);                
            if($request->status){
                $results = $results->whereIn("status",$request->status);
            }
            $results = $results->get();
            $result = array_add($result,$cust->name,$results);
            
        }
        // return $result;
        $dated = Carbon::now()->format("Y-m-d");        
        if($request['start']!=null && $request['end']!=null){
            $dated=$request['start']." - ".$request['end'];
        }
        else {
            if($request['end']!=null)
                $dated= "~".$request['end'];                     
            else if($request['start']!=null)
                $dated= $request['start']."~";                
        }
        // return view('report.customer')->with(['result' => $result, 'dated'=>$dated]);
        $pdf = SnappyPdf::loadView('report.customer', ['result' => $result, 'dated'=>$dated]);
        // ->setPaper('letter','landscape');
        return $pdf->download('Laporan Order Per Customer['. $dated .'].pdf');
    }

    public function facility(Request $request){
        // return $request;
        // $notin = Order::whereDate("date","<",$request->filter['start'])->orwhereDate("date",">",$request->filter['end']);
        $fcstyp = FacilityType::all();
        $result=[];
        foreach($fcstyp as $facs){
            $results = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->where("item_type","LIKE","%Facility")->whereIn("item_id",$facs->facility->pluck("id"));
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end); 
            if($request->status){
                $results = $results->whereIn("orders.status",$request->status);
            }
            $results = $results->selectRaw("detail_orders.id,price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname")->get();
            $result = array_add($result,$facs->name,$results);
        }
            $rest=[];
            $results = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->where("item_type","LIKE","%Bundle");
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end); 
            if($request->status){
                $results = $results->whereIn("orders.status",$request->status);
            }
            $results = $results->selectRaw("detail_orders.id,price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname")->get();
            $rest = array_add($rest,"Bundle",$results);
        
             $results = DetailOrder::join('orders', 'orders.id', '=', 'detail_orders.order_id')->join('customers', 'customers.id', '=', 'orders.customer_id')->where("item_type","LIKE","%Addon");
            if($request->start!=null)
                $results = $results->whereDate("date",">=",$request->start);
            if($request->end!=null)
                $results = $results->whereDate("date","<=",$request->end); 
            if($request->status){
                $results = $results->whereIn("orders.status",$request->status);
            }
            $results = $results->selectRaw("detail_orders.id,price,amount,amount2,item_type,item_id ,detail_orders.disc, orders.date, orders.order_code, customers.name cname")->get();
            $rest = array_add($rest,"Addon",$results);
        
            // return $result;
        
        // return $result;
        $dated = Carbon::now()->format("Y-m-d");        
        if($request['start']!=null && $request['end']!=null){
            $dated=$request['start']." - ".$request['end'];
        }
        else {
            if($request['end']!=null)
                $dated= "~".$request['end'];                     
            else if($request['start']!=null)
                $dated= $request['start']."~";                
        }
        // return view('report.facility')->with(['result' => $result,'rest' => $rest, 'dated'=>$dated]);
        $pdf = SnappyPdf::loadView('report.facility', ['result' => $result,'rest' => $rest, 'dated'=>$dated]);
        // ->setPaper('letter','landscape');
        return $pdf->download('Laporan Order Per Fasilitas['. $dated .'].pdf');
        
    }
}
