<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\CustomSchedule;
use Illuminate\Http\Request;
use App\DetailFacility;
use App\Folio;
use App\Order;
use App\DetailOrder;
use App\User;

class DetailFacilityCustomScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, DetailFacility $detail_facility)
    {
        $custom_schedule = CustomSchedule::query();
        $folios = Folio::query();      
        $custom_schedule = $custom_schedule->where('detail_facility_id', '=', $detail_facility->id);
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value) && $key=='startdate') {
                    $startdate=Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
                    $custom_schedule = $custom_schedule->where($key, '>=', $startdate);
                    $folios = $folios->whereDate("check_in", '>=', $startdate);
                }
                if(!empty($value) && $key=='enddate') {
                    $enddate=Carbon::createFromFormat('Y-m-d', $value)->endOfDay();
                    $custom_schedule = $custom_schedule->where($key, '<=', $enddate);
                    $folios = $folios->whereDate("check_out", '>=', $startdate);                    
                }
            }
        }
        $carbonevent = array();
        // return $custom_schedule;
        $folios = $folios->where('folio_id',$detail_facility->id)->join("detail_facilities","folio_id","detail_facilities.id")->join("detail_orders","detail_order_id","detail_orders.id")->join("orders","order_id","orders.id")->join("customers","customer_id","customers.id")->where("folio_type","LIKE","%DetailFacility")->selectRaw("null as id,check_in as start, check_out as end, detail_facilities.name as facility, customers.name as description, orders.id as order_id, orders.order_code as order_code");
        $custom_schedule = $custom_schedule->join("detail_facilities","detail_facility_id","detail_facilities.id")->selectRaw("custom_schedules.id, startdate as start, enddate as end, detail_facilities.name as facility, description, null as order_id, null as order_code")->union($folios)->get();
        // return $carbonevent;
        return view('detail_facilities_custom_schedules.index', compact('custom_schedule', 'filter', 'detail_facility'));
    }

    public function getCalendar(Request $request)
    {
        // return $request;
        $carbonevent = array();
        $event = CustomSchedule::all(); 
        $folios = Folio::where("folio_type","LIKE","%DetailFacility")->orderBy("updated_at","DESC")->limit(3)->get();
        $usr= new User;  
        $usr->role=1;    
        if($request->id!="all"){
            
            $event = CustomSchedule::where('detail_facility_id', '=', $request->id)->get();
            $folios = Folio::where('folio_id',$request->id)->where("folio_type","LIKE","%DetailFacility")->get();
        }
        else{
            $usr = User::find($request->auth);
            // return $usr->role;
        }
           for($i=0; $i<count($event); $i++){
            $carbonevent[$i] = array();
            $carbonevent[$i][0]=$event[$i]->detailFacility->facility->name.' '.$event[$i]->detailFacility->name;

            $carbonevent[$i][1]=$event[$i]->description;
            $carbonevent[$i][2]="#57585b";
            $carbon = new Carbon($event[$i]->startdate);
            $carbonevent[$i][3]=$carbon->toDateTimeString();
            $carbon = new Carbon($event[$i]->enddate);
            $carbonevent[$i][4]=$carbon->toDateTimeString();
            $carbonevent[$i][5]="#";            
            if($usr->role==1){
            $carbonevent[$i][5]=route('detail_facilities.custom_schedules.index',['detail_facility'=>$event[$i]->detailFacility->id]);                
            }
            // $carbonevent[$i][5]=route('detail_facilities.custom_schedules.edit',['detail_facility'=>$event[$i]->detailFacility->id,'custom_schedule'=>$event[$i]->id]);
            
        }
        // return $carbonevent;
        foreach($folios as $folio){
            $tmp = [];
            $ord= Order::find($folio->detail_order->order_id);            
            if($folio->folio_type=="App\\DetailFacility"){
                $detf = DetailFacility::find($folio->folio_id);   
                $tmp[]=$detf->facility->name." ".$detf->name;   
                if($ord->status<2){
                    $tmp[]= "Booking Oleh ".$ord->customer->nameLabel;
                    $tmp[]="#d67c00";
                }
                else {
                    $tmp[]= "Digunakan Oleh ".$ord->customer->nameLabel;
                    $tmp[]="#f56954";                     
                }   
                 $tmp[]= $folio->check_in->toDateTimeString();
                $tmp[]= $folio->check_out->addDay()->toDateTimeString(); 
                            
                if($usr->role==1){
                    $tmp[] = route('orders.show',$ord->id);  
                }    
                else{
                    $tmp[]="#";
                }               
            }

            $carbonevent[] = $tmp;
        }

        return $carbonevent;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(DetailFacility $detail_facility)
    {
        $custom_schedule = new CustomSchedule;
        return view('detail_facilities_custom_schedules.create', compact('custom_schedule', 'detail_facility'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DetailFacility $detail_facility)
    {
        $custom_schedule = new CustomSchedule;
        $array=explode('-', $request->rangedate);
        $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
        $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
        $custom_schedule->startdate=$startdate;
        $custom_schedule->enddate=$enddate;
        $custom_schedule->fill($request->all());
        $custom_schedule->detail_facility_id=$detail_facility->id;
        $custom_schedule->save();
        $request->session()->flash('toast', 'Jadwal berhasil ditambahkan!');
        
        return redirect('/detail_facilities/'.$detail_facility->id.'/custom_schedules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(DetailFacility $detail_facility)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailFacility $detail_facility, CustomSchedule $custom_schedule)
    {
        return view('detail_facilities_custom_schedules.create', compact('custom_schedule', 'detail_facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailFacility $detail_facility, CustomSchedule $custom_schedule)
    {
        $array=explode('-', $request->rangedate);
        $startdate=Carbon::createFromFormat('d/m/Y H:i:s ', $array[0]);
        $enddate=Carbon::createFromFormat(' d/m/Y H:i:s', $array[1]);
        $custom_schedule->startdate=$startdate;
        $custom_schedule->enddate=$enddate;
        $custom_schedule->fill($request->all());
        $custom_schedule->save();
        $request->session()->flash('toast', 'Jadwal berhasil diubah!');
        
        return redirect('/detail_facilities/'.$detail_facility->id.'/custom_schedules');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DetailFacility $detail_facility, CustomSchedule $custom_schedule)
    {
        try {
            $custom_schedule->delete();
            $request->session()->flash('toast', 'Jadwal berhasil dihapus!');
        } catch(\Illuminate\Database\QueryException $ex){ 
            $request->session()->flash('toast', 'Jadwal gagal dihapus. ' . substr($ex->getMessage(), 0, 15));
        }
        
        return redirect('/detail_facilities/'.$detail_facility->id.'/custom_schedules');
    }
}
