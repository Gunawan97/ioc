<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Folio;
use App\DetailFacility;
use App\Addon;
use Illuminate\Support\Facades\Auth;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Order;
use Carbon\carbon;
class FolioController extends Controller
{
    public function index(Request $request){
        $folios = Folio::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                     if($key=="check_in") {
                        $folios  = $folios->whereDate("check_in",">=",$value);
                    }
                    else{
                        $folios  = $folios->whereDate("check_out","<=",$value);
                    }
                }
            }
        }
        $fac= clone $folios;
        $fac = $fac->where("folio_type","LIKE","%DetailFacility")->orderBy('id','DESC')->paginate(10);        
        $add=$folios->where("folio_type","LIKE","%Addon")->orderBy('id','DESC')->paginate(10);
        
        return view('folio.index')
        ->with('order',"all")
        ->with('adds',$add)
        ->with('facs',$fac)
        ->with('facs',$fac)
        ->with('filter',$filter);
    }
    public function create(){
        $folio = new Folio;
        return view('folio.create')->with('folio',$folio);
    }
    public function store(Request $request){
        $folio = new Folio;
        $folio->fill($request->all());
        $folio->save();
        $request->session()->flash('toast', 'Folio berhasil ditambahkan!');
        return redirect()->route('folios.index');
    }
    public function edit($id){
        $folio = Folio::find($id);
        return view('folio.edit')->with('folio',$folio);
    }
    public function update($id, Request $request){
        $folio = Folio::find($id);
        $folio->fill($request->all());
        $folio->save();
        $request->session()->flash('toast', 'Folio  berhasil diubah!');
        return redirect()->route('folios.index');
    }
    public function show(Folio $folio){
        return view('folio.show')->with('folio',$folio);
    }
    public function destroy(Request $request, Folio $folio){
        $folio->delete();
        $request->session()->flash('toast', 'Folio berhasil dihapus!');
        $folio->delete();
        return redirect()->route('folios.index');
    }

    public function getCalendar(Request $request)
    {
        $carbonevent = array();
        $folios = Folio::where("active",1)->where("folio_type","App\\DetailFacility");                    
        if($request->id!="all"){
            $ord = Order::find($request->id);
            $folios = $folios->whereIn("detail_order_id",$ord->detail_order->pluck("id"));
        }
        $folios=$folios->get();
        // return $carbonevent;
        foreach($folios as $folio){
            $tmp = [];
            $ord= Order::find($folio->detail_order->order_id);            
            if($folio->folio_type=="App\\DetailFacility"){
                $detf = DetailFacility::find($folio->folio_id);   
                $tmp[]=$detf->facility->name." ".$detf->name;   
                if($ord->status<2){
                    $tmp[]= "Booking Oleh ".$ord->customer->nameLabel;
                    $tmp[]="#d67c00";
                }
                else {
                    $tmp[]= "Digunakan Oleh ".$ord->customer->nameLabel;
                    $tmp[]="#f56954";                     
                }   
                $tmp[]= $folio->check_in->toDateTimeString();
                $tmp[]= $folio->check_out->addDay()->toDateTimeString();
                $tmp[] = route('orders.show',$ord->id);
            }

            
           
            // return $tmp;
            $carbonevent[] = $tmp;
        }

        return $carbonevent;
    }

    public function print(Request $request)
    {
                // return $request;
  
        // $notin = Order::whereDate("date","<",$request->filter['start'])->orwhereDate("date",">",$request->filter['end']);
        $facs = Folio::where("folio_type","LIKE","%Facility")->get();
        $addons = Folio::where("folio_type","LIKE","%Addon%")->get();
        $result=[];
        // return $result;
        $dated = Carbon::now()->format("Y-m-d");        
        if($request['start']!=null && $request['end']!=null){
            $dated=$request['start']." - ".$request['end'];
        }
        else {
            if($request['end']!=null)
                $dated= "~".$request['end'];                     
            else if($request['start']!=null)
                $dated= $request['start']."~";                
        }
        // return view('folio.print')->with(['facs' => $facs,'addons' => $addons, 'dated'=>$dated]);
        $pdf = SnappyPdf::loadView('folio.print', ['facs' => $facs,'addons' => $addons,  'dated'=>$dated]);
        // ->setPaper('letter','landscape');
        return $pdf->download('Folio ['. $dated .'].pdf');
    }
}
