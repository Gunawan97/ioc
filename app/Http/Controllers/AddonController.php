<?php

namespace App\Http\Controllers;

use App\Addon;
use App\Unit;
use Illuminate\Http\Request;

class AddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $addon = Addon::query();
        
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    $addon = $addon->where($key, 'like', '%'.$value.'%');
                }
            }
        }
        
        $addon = $addon->paginate(10);
        return view('addons.index', compact('addon', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $addon = new Addon;
        $unit = Unit::pluck('name', 'id')->all();
        return view('addons.create', compact('addon', 'unit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addon = new Addon;
        $addon->fill($request->all());
        $addon->save();
        $addon->slug = str_slug($addon->id.'_'.$addon->name, '_');
        $addon->save();
        $request->session()->flash('toast', 'Addon berhasil ditambahkan');
        return redirect('/addons');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function show(Addon $addon)
    {
        return view('addons.show', compact('addon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function edit(Addon $addon)
    {
        $unit = Unit::pluck('name', 'id')->all();
        return view('addons.create', compact('addon', 'unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Addon               $addon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Addon $addon)
    {
        $addon->fill($request->all());
        $addon->slug = str_slug($addon->id.'_'.$addon->name, '_');
        $addon->save();
        $request->session()->flash('toast', 'Addon berhasil diubah');
        return redirect('/addons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Addon $addon)
    {
        try{
            $addon->delete();
            $request->session()->flash('toast', 'Addon berhasil dihapus!');
        }
        catch(\Illuminate\Database\QueryException $ex){
            $request->session()->flash('toast', 'Addon gagal dihapus'.substr($ex->getMessage(), 0, 15));

        }
        return redirect('/addons');
    }

    //api
    public function get(Request $request)
    {
        $add = Addon::where("name", "LIKE", "%".$request->q."%")->get();
        return $add;
    }
}
