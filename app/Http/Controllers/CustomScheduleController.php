<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\CustomSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class CustomScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $custom_schedule = CustomSchedule::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value) && $key=='startdate') {
                    $startdate=Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
                    $custom_schedule = $custom_schedule->where($key, '>=', $startdate);
                }
                if(!empty($value) && $key=='enddate') {
                    $enddate=Carbon::createFromFormat('Y-m-d', $value)->endOfDay();
                    $custom_schedule = $custom_schedule->where($key, '<=', $enddate);
                }
            }
        }
        $custom_schedule = $custom_schedule->paginate(5);
        return view('custom_schedules.index', compact('custom_schedule', 'filter'));
    }

    public function getCalendar()
    {
        $carbonevent = array();
        $event = CustomSchedule::all();
        for($i=0; $i<count($event); $i++){
            $carbonevent[$i] = array();
            $carbonevent[$i][0]=$event[$i]->detailFacility->facility->name.' '.$event[$i]->detailFacility->name;
            $carbonevent[$i][1]=$event[$i]->description;
            $carbon = new Carbon($event[$i]->startdate);
            $carbonevent[$i][2]=$carbon->toDateTimeString();
            $carbon = new Carbon($event[$i]->enddate);
            $carbonevent[$i][3]=$carbon->toDateTimeString();
        }
        return $carbonevent;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomSchedule $customSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(CustomSchedule $custom_schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomSchedule $customSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomSchedule $custom_schedule)
    {
        // return view('detail_facilities_custom_schedules.create', compact('custom_schedule', 'detail_facility'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CustomSchedule      $customSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomSchedule $custom_schedule)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomSchedule $customSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomSchedule $custom_schedule, Request $request)
    {
        
    }
}
