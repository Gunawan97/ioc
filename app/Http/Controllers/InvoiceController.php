<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Order;
use App\DetailInvoice;
use App\DetailOrder;
use App\User;
use App\Mail\Email;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
  
    public function index($order, Request $request)
    {   
        
        $invoices = Invoice::query();
        $filter = [];
        $invoices = $invoices->where("order_id",$order);
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                     if($key=="start") {
                        $invoices  = $invoices->whereDate("date", '>=', $value);
                    }
                    else{
                        $invoices  = $invoices->whereDate("date", '<=', $value);
                    }
                }
            }
        }
        $invoices= $invoices->paginate(20);
        $ordcod = Order::find($order);
        return view('invoice.index')
        ->with('order', $order)
        ->with('filter', $filter)
        ->with('ordcod', $ordcod)
        ->with('invoices', $invoices);
    }
    public function create($order){
        
        $id = $order;
        $invoice = new Invoice;
        $invoice->order_id = $order;
        $orders = Order::all()->pluck("OrderLabel", "id");
        $facs = Order::where("id", $order)->get();
        $dets = DetailOrder::all()->pluck("selectLabel", "id");
        
        return view('invoice._form')
        ->with('dets', $dets)
        ->with('ords', $facs)
        ->with('orders', $orders)
        ->with('invoice', $invoice);
    }
    public function store(Request $req)
    {    
        // return $req;
        $invoice = new Invoice;
        $invoice->fill($req->except("facil", "amount","description","ids"));
        $date_start = $invoice->date->startOfMonth();
        $date_end = $invoice->date->endOfMonth();
        $invoice_counter = Invoice::where('order_id', $invoice->order_id)->whereBetween('date', [$date_start,$date_end])->count();        
        $invoice->invoice_code = "INV/".str_pad($invoice->order_id,3,"0",STR_PAD_LEFT)."/".$invoice->date->format("d/m/y")."/".str_pad($invoice_counter,3,"0",STR_PAD_LEFT);
        
        $invoice->save();
        $i=0;
        $total=0;
        foreach ($req->description as $fac) {
            $din = new DetailInvoice;
            $din->amount= $req->amount[$i];
            $din->description= $req->description[$i];
            $din->invoice_id = $invoice->id;
            $din->save();
            $total +=$req->amount[$i];
            $i++;
        }

        $order = Order::find($req->order_id);
        if($order->status==0){
            $order->status=1;
            $user= User::where("emailable",1)->get();
            foreach($user as $usr){
                Mail::to($usr->email)->send(new Email($order));
                if(Mail::failures()){
                // $string="There is some Errors, Try Again Later";
                }  
            }
        }
        if($order->lunas==0){
            $order->lunas=1;
        }
        if($order->payableLabel <=0){
            $order->lunas=2;
        }
        $order->save();

        return redirect()->route('orders.invoices.index',["order"=>$order->id]);
    }
    public function edit($id){
        $invoice = Invoice::find($id);
        $orders = Order::all()->pluck("selectLabel", "id");
        $facs = Order::where("id", $invoice->order_id)->get();        
        
        return view('invoice._form')
        ->with('orders', $orders)
        ->with('ords', $facs)        
        ->with('invoice', $invoice);
    }
    public function update($id, Request $req){
        $invoice = Invoice::find($id);
         $invoice->fill($req->except("facil", "amount","descriptions","ids"));

        $invoice->save();
        $i=0;
        $total=0;
        $deldetin = DetailInvoice::where("invoice_id",$invoice->id)->delete();
        foreach ($req->descriptions as $fac) {
            $din = new DetailInvoice;
            $din->amount= $req->amount[$i];
            $din->description= $req->descriptions[$i];
            $din->invoice_id = $invoice->id;
            $din->save();
            $total +=$req->amount[$i];
            $i++;
        }

        $order = Order::find($req->order_id);
        if($order->status==0){
            $order->status=1;
            $user= User::where("emailable",1)->get();
            foreach($user as $usr){
                Mail::to($usr->email)->send(new Email($order));
                if(Mail::failures()){
                // $string="There is some Errors, Try Again Later";
                }                
            }
        }
        if($order->lunas==0){
            $order->lunas=1;
        }
        if($order->payableLabel <=0){
            $order->lunas=2;
        }

        return redirect()->route('orders.invoices.index',$invoice->order_id);
    }
    public function show(Invoice $invoice){   
        return view('invoice.show')->with('invoice', $invoice);
    }
    public function destroy(Invoice $invoice){
        DetailInvoice::where("invoice_id",$invoice->id)->delete();
        $ordid=$invoice->order_id;
        $invoice->delete();
        return redirect()->route('orders.invoices.index',$ordid);
    } 
   
}
