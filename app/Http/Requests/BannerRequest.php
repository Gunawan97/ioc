<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image_desktop'=>'required|image|mimes:jpeg,png,jpg|max:2048',
            'image_mobile'=>'required|image|mimes:jpeg,png,jpg|max:2048',
            'title'=>'required|string',
            'subtitle'=>'required|string',
            'description'=>'required|string',
            'link_text'=>'required|string',
            'link_url'=>'required|string',
            'banner_location_id'=>'required'
        ];
    }

    public function attributes()
    {
        return [
            'image_desktop'=>'Gambar Versi Desktop',
            'image_mobile'=>'Gambar Versi Mobile',
            'title'=>'Judul',
            'subtitle'=>'Subjudul',
            'description'=>'Deskripsi',
            'link_text'=>'Tulisan Link',
            'link_url'=>'URL Link',
            'banner_location_id'=>'Lokasi Banner'
        ];
    }
    
    public function messages()
    {
        return [
            'required' => ':attribute harus diisi',
            'max' => ':attribute terlalu besar'
        ];
    }
}
