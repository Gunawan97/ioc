<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponFacilityType extends Model
{
    public function coupon()
    {
        return $this->BelongsTo('App\Coupon');
    }
    public function facility_type()
    {
        return $this->BelongsTo('App\FacilityType');
    }
    
}
