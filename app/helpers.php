<?php

namespace App;
use Carbon\carbon;
use App\GlobalSetting;

class Helpers
{
    public static function globs($key,$val=null)
    {
        if($val!=null) {
            $glob=GlobalSetting::where("key", "LIKE", "%".$key."%")->first();
            $glob->value=$val;
            $glob->save();
        }
        else{
            $glob=GlobalSetting::where("key", "LIKE", "%".$key."%")->first();
            return $glob->value;
        }
    }

    public static function rupiah($nums)
    {
        return 'Rp. ' . number_format( $nums, 0 , '' , '.' );
    }
}
?>