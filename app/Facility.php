<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailFacility;
use Carbon\carbon;
use App\CustomPrice;
use App\SpecialPrice;
class Facility extends Model
{
    protected $guarded =[];

    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleriable');
    }
    public function facilityType()
    {
        return $this->belongsTo('App\FacilityType', 'type_id');
    }
    public function facilityDetail()
    {
        return $this->hasMany('App\FacilityDetail', 'facility_id');
    }
    public function detailFacility()
    {
        return $this->hasMany('App\DetailFacility', 'facility_id');
    }
    public function special_price()
    {
        return $this->morphMany('App\SpecialPrice', 'special');
    }
    public function custom_price()
    {
        return $this->morphMany('App\CustomPrice', 'price');
    }
    public function detail_order()
    {
        return $this->morphMany('App\DetailOrder', 'item');
    }
    public function bundle_detail()
    {
        return $this->morphMany('App\BundleDetail', 'bundl');
    }
    public function coupon_facility()
    {
        return $this->morphMany('App\CouponFacility', 'facs');
    }

    public function getunitLabelAttribute()
    {
        return $this->unit == 1 ? "jam" : "hari";
    }
    public function getUnitNameAttribute()
    {
        return $this->unit == 1 ? "Jam" : "Hari";
    }

    public function calPrice($date,$dur,$person=1,$disc=0,$stay=0,$custype=0){
        $start= Carbon::createFromFormat("Y-m-d",$date);
        $total=0;
        for($i=0;$i<$dur;$i++){
            $curdate = $start->addDays(1)->format("Y-m-d");
            $total += $this->todayPrice($curdate,$stay,$custype);
        }
        if($this->per_person==1 && $this->minimum>$person){
            $person = $this->minimum;
        }
        $total *=$person;                    
        $total -= $total * $disc/100;
        return $total;
    }

    public function todayPrice($date,$stay=0,$custtype=0){
        $dt = Carbon::createFromFormat("Y-m-d",$date);
        $cusp = CustomPrice::whereDate("startdate","<=",$date)->whereDate("enddate",">=",$date)->where("price_id",$this->id)->where("price_type","LIKE","%Facility")->first();
        if($cusp)
            return $cusp->price;
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Facility")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        if($dt->isWeekend())        
                        {return $cus->no_stay_weekend_price;}
                        else        
                        {return $cus->no_stay_weekday_price;}
                    }
                    else if($stay==1){
                        if($dt->isWeekend())        
                            {return $cus->weekend_price;}
                        else        
                            {return $cus->price;}
                    }
                }
            }
        }
        if($stay==0){
            if($dt->isWeekend())        
                {return $this->no_stay_weekend_price;}
            else        
                {return $this->no_stay_weekday_price;}
        }
        if($dt->isWeekend() && $this->weekend_price!=null)        
                {return $this->weekend_price;}
        return $this->price;
    }
    public function Available($date,$dur){
        $det = DetailFacility::where("facility_id",$this->id)->rentable($date,$dur)->get();
        return $det;
    }

    protected $appends = ['unit_name'];
}
