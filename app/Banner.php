<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $guarded = ['image_desktop','image_mobile'];

    public function bannerLocation()
    {
        return $this->belongsTo('App\BannerLocation', 'banner_location_id');
    }
}
