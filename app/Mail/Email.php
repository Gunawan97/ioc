<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use H;
class Email extends Mailable
{
    use Queueable, SerializesModels;
    public $ord;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {

        $this->ord = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $level='info';
        $cus=$this->ord->customer->nameLabel;
        $mes = "<p>it's a</p><h3>Trap!</h3>";
        $sub ="[IOC] ";                  
        if($this->ord->status==0){
            $intro=" Membuat Booking!";  
            $sub .= $cus." Membuat Booking Pada Sistem IOC!";
        }
        else if($this->ord->status==1){
            $intro=" Melakukan Konfrimasi Booking! Dengan membayar biaya sebesar ".H::rupiah($this->ord->paidLabel);
            $sub .= $cus." Melakukan Konfirmasi Booking Pada Sistem IOC!";
                        
        }
        else if($this->ord->status==2){
            $intro=" Melakukan Check-in!";     
            $sub .= $cus." Melakukan Check-in Pada Sistem IOC!";
                   
        }
        else if($this->ord->status==3){
            $intro=" Melakukan Check-out!";
            $sub .= $cus." Melakukan Check-out Booking Pada Sistem IOC!";                     
        }
        else if($this->ord->status==4){
            $intro=" Melakukan Pembatalan Booking!"; 
            $sub .= $cus." Melakukan Pembatalan Booking Pada Sistem IOC!";
                       
        }
        else if($this->ord->status==5){
            $intro=" Melakukan Perubahan Pada Booking!"; 
            $sub .= $cus." Melakukan Perubahan Booking Pada Sistem IOC!";
                       
        }
        
        $outro=[
            "Segera balas pesan ".$this->ord->customer->nameLabel." ".$this->ord->lname." melalui ".$this->ord->email." atau dengan menghubungi ".$this->ord->phone."."];

        return $this->subject($sub)->markdown('orders.email')->with([
                'level'=>$level, 
                'introLines'=>$intro,
                'outroLines'=>$outro,
                'cus'=>$cus,
                'mes'=>$mes
            ]);
    }
}
