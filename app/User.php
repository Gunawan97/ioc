<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function news()
    {
        return $this->hasMany('App\News', 'user_id');
    }
    protected $fillable = [
        'name', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRoleNameAttribute()
    {
        if($this->role == 1) {
            return "Administrator";
        }
        else if($this->role == 2) {
            return "Supervisor";
        }
        else if($this->role == 3) {
            return "Staff";
        }
    }

    protected $appends = ['role_name'];
    protected $guarded = [];
}
