<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\carbon;

class Bundle extends Model
{
    protected $guarded =[];

    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleriable');
    }
    public function bundle_detail()
    {
        return $this->hasMany('App\BundleDetail');
    }
    public function coupon_facility()
    {
        return $this->morphMany('App\CouponFacility', 'facs');
    }
    public function detail_order()
    {
        return $this->morphMany('App\DetailOrder', 'item');
    }
    public function special_price()
    {
        return $this->morphMany('App\SpecialPrice', 'special');
    }
    public function custom_price()
    {
        return $this->morphMany('App\CustomPrice', 'price');
    }

    public function calPrice($date,$dur,$person=1,$disc=0,$stay=0,$custype=0){
        // return $custype;
        $start= Carbon::createFromFormat("Y-m-d",$date);
        // $end = $start->addDays($dur);
        $total=0;
        // $vl=[];
        for($i=0;$i<$dur;$i++){
            $curdate = $start->addDays(1)->format("Y-m-d");
            $total += $this->todayPrice($curdate,$stay,$custype);
            // $vl=array_add($vl,$curdate,$this->todayPrice($curdate));
        }
        // return $vl;
        
        $total *=$person;
        $total -= $total * $disc/100;
        return $total;
    }

    public function todayPrice($date,$stay=0,$custtype=0){
        $dt = Carbon::createFromFormat("Y-m-d",$date);
        $cusp = CustomPrice::whereDate("startdate","<=",$date)->whereDate("enddate",">=",$date)->where("price_id",$this->id)->where("price_type","LIKE","%Bundle")->first();
        if($cusp)
            return $cusp->price;
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Bundle")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        if($dt->isWeekend())        
                        {return $cus->no_stay_weekend_price;}
                        else        
                        {return $cus->no_stay_weekday_price;}
                    }
                    else if($stay==1){
                        if($dt->isWeekend())        
                            {return $cus->weekend_price;}
                        else        
                            {return $cus->price;}
                    }
                }
            }
        }
        if($stay==0){
            if($dt->isWeekend())        
                {return $this->no_stay_weekend_price;}
            else        
                {return $this->no_stay_weekday_price;}
        }
        if($dt->isWeekend() && $this->weekend_price!=null)        
                {return $this->weekend_price;}
        {return $this->price;}
    }


}
