<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/', 'HomeController@index'
);

Route::Group(
    ['middleware'=>'auth'], function () {
        Route::resource('orders.invoices','InvoiceController');        
        Route::resource('/orders', 'OrderController');
        Route::get('/report/customer', 'ReportController@customer')->name('reports.customer');
        Route::get('/report/facility', 'ReportController@facility')->name('reports.facility');
        Route::get('/pint/folio', 'FolioController@print')->name('folios.print');
        Route::resource('/reports','ReportController');
        Route::resource('/invoices', 'InvoiceController');

        Route::resource('/customers', 'CustomerController');
        Route::resource('/customer_types', 'CustomerTypeController');
        Route::resource('/users', 'UserController');
        Route::resource('/galleries', 'GalleryController');
        Route::resource('/facility_attributes', 'FacilityAttributeController');
        Route::resource('/addons', 'AddonController');
        Route::resource('/facility_details', 'FacilityDetailController');
        Route::resource('/facilities', 'FacilityController');
        Route::resource('/facility_types', 'FacilityTypeController');
        Route::resource('/custom_schedules', 'CustomScheduleController');
        Route::resource('/units', 'UnitController');
        Route::resource('/facilities.galleries', 'FacilityGalleryController');
        Route::resource('/facilities.facility_details', 'FacilityFacilityDetailController');
        Route::resource('/coupons', 'CouponController');
        Route::resource('/news', 'NewsController');
        Route::resource('/categories', 'CategoryController');
        Route::resource('/banners', 'BannerController');
        Route::resource('/testimonies', 'TestimonyController');
        Route::resource('/custom_prices', 'CustomPriceController');
        Route::resource('/education_tours', 'EducationTourController');
        Route::resource('/education_tours.galleries', 'EducationTourGalleryController');
        Route::resource('/bundles.galleries', 'BundleGalleryController');
        Route::resource('/facilities.detail_facilities', 'FacilityDetailFacilityController');
        Route::resource('/facilities.custom_prices', 'FacilityCustomPriceController');
        Route::resource('/bundles', 'BundleController');
        Route::resource('/detail_facilities.custom_schedules', 'DetailFacilityCustomScheduleController');
    
        Route::get('/news_image/{news}', 'NewsController@editImage')->name('news.editImage');
        Route::put('/news_image/{news}', 'NewsController@updateImage')->name('news.updateImage');
        Route::get('/order/{order}/folios', 'OrderController@folio')->name('orders.folios.index');
    
        Route::resource('/bundlings', 'BundlingController');
        Route::resource('/bundlings.facilities', 'BundlingFacilityController');
        Route::resource('/folios', 'FolioController');
        Route::resource('/special_prices', 'SpecialPriceController');
        Route::resource('orders.order_documents','OrderDocumentController');
    }
);
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/order/{id}/invoices/create/{invoice]', 'InvoiceController@create')->name('order.invoice.create.ord');
Route::post('/order/stat','OrderController@stat')->name("orders.stat");
