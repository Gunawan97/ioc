@extends('layouts.app')

@section('title')
Daftar Jurusan
@endsection

@push('styles')
<style type="text/css">

</style>
@endpush

@section('content')

<!-- START OF TABLE -->
<div id="tabel">
  <div class="row">
    <!-- Search Jurusan -->
    <form method="get" id="search-form">
      <div class="col-md-3">
        <div class="input-group no-border">
          <input type="text" class="form-control column-filter" name="filter[name]" placeholder="Jurusan" value="{{ !empty($filter['name']) ? $filter['name'] : '' }}"/>
          <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
        </div>
      </div>

      <div class="col-md-3">
        <button type="submit" class="btn btn-primary btn-block">Cari</button>
      </div>
    </form>
  </div>

  <div class="box">
    <div class="box-header">
      <div class="row">
        <div class="col-md-3">
          <a href="{{ route('majors.create')}}" class="btn btn-success btn-block">Tambah Jurusan &nbsp;<span class="fa fa-plus-circle"></span></a>
        </div>
      </div>
    </div>

    <!-- BOX START-->
    <div class="box-body">
      <div class="row">
        <div class="col col-md-12">
          <table class="table table-striped table-bordered dataTable">
            <thead>
              <tr>
                <th class="align-middle">No</th>
                <th class="align-middle">Nama</th>
                
                <th class="align-middle">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($majors as $major)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $major->name }}</td>
                
                <td><a class="text-view edit-action" href="{{ route('majors.edit',$major->id) }}">Edit</a> <span class="fa fa-chevron-circle-right"></span></td>
              </tr>
              @endforeach
              @if($majors->count() == 0)
              <tr>
                <td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
              </tr>
              @endif
            </tbody>
          </table>
          <div class="row">
              <div class="col-md-12">
                {!! H::paginationSummary($majors) !!}
              </div>
          </div>
          @php
            $filters = [];
            foreach ($filter as $key => $value) {
              $filters['filter['. $key .']'] = $value;
            }
          @endphp
          {{ $majors->appends($filters)->links() }}
        </div>
      </div>
    </div>
  </div>

</div>

<!-- Calon Siswa End-->
@endsection
