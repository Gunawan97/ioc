<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  @include("layouts._head")

  @stack('styles')
  <link rel="stylesheet" href="{{ asset('css/skins/skin-black.min.css') }}">
  <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{{ mix('css/custom.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
  <link rel="stylesheet" href="{{ asset('js/plugins/datepicker/datepicker3.css') }}" media="screen,projection">
  
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="{{ route('psb.dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SSIS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Sinlui <b>SIS</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      @include('layouts._navbar_menu')

      
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- Notifications Menu -->
          {{-- <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                Inner Menu: contains the notifications
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li> --}}
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{ asset('images/sinlui-bg.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">

                <p>
                  {{ Auth::user()->name }}
                  {{-- <small>Administrator</small> --}}
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <!-- <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div> -->
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Sign out
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('images/sinlui-bg.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <!-- Status -->
          {{-- <a href="{{ route('dashboard') }}">Merchant</a> --}}
        </div>
      </div>

      @include('layouts._sidebar_menu')
      
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class='row'>
        <div class="col-md-6">
          <h1 class="title-header">
            @yield('title')
          </h1>
        </div>
        <div class="col-md-6 text-right">
          @yield('actionbtn')
        </div>
      </div>
      
      
      <!-- <ol class="breadcrumb"> -->
        
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li> -->
      <!-- </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
         @if (Session::has('toast'))
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-check"></i> Success!</h4>
          {!! session('toast') !!}
        </div>
        @endif
      @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('layouts._footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- Script -->
<script src="{{ mix('js/app.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script type="text/javascript"> 
  // Date Picker
  $('.datepicker').datepicker({
    format:'yyyy-mm-dd',
    autoclose:true,
    todayBtn:true,
    maxDateNow: true,
  });
</script>
@stack('scripts')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
