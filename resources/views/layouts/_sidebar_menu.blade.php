  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('image/Logo_Warna_Berdiri_400x400.jpg')}}"" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <p>{{ Auth::user()->role_name }}</p>
          {{--  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>  --}}
          <br>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ active('home') }}">
          <a href="{{ route('home') }}" >
            <i class="fa fa-dashboard"></i>
            <span>Dasboard</span>
          </a>
        </li>
        <li class="treeview {{active('custom_schedules.*')}} {{active('folios.*')}} ">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Jadwal</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li class="{{active('folios.*')}}">
              <a href="{{ route('folios.index') }}" >
                <i class="fa fa-book"></i>
                <span>Jadwal Sewa</span>
              </a>
            </li>
            <li id="customSchedule" class="{{active('custom_schedules.*')}}">
              <a href="/custom_schedules">
                <i class="fa fa-wrench"></i>
                <span>Jadwal Lain-lain</span>
              </a>
            </li>
          </ul>
        </li>
        @if(Auth::user()->role==1 )        
        
        <li id="customerPage" class="{{active('customers.*')}}">
          <a href="/customers">
            <i class="fa fa-users"></i>
            <span>Customer</span>
          </a>
        </li>
        <li class="{{active('testimonies.*')}}">
          <a href="{{ route('testimonies.index') }}" >
            <i class="fa fa-comments"></i>
            <span>Testimony</span>
          </a>
        </li>
        <li  id="newsPage" class="treeview {{active('news.*')}} {{active('categories.*')}}">
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Manage Berita</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li id="newsPage" class="{{active('news.*')}}">
              <a href="/news">
                <i class="fa fa-quote-right"></i>
                <span>News</span>
              </a>
            </li>

            <li id="categoryPage" class="{{active('categories.*')}}">
              <a href="/categories">
                <i class="fa fa-cube"></i>
                <span>Category</span>
              </a>
            </li>
          </ul>
        </li>


<!--         <li id="customPrice" class="{{active('custom_prices.*')}}">
          <a href="/custom_prices">
            <i class="fa fa-dollar"></i>
            <span>Custom Price</span>
          </a>
        </li> -->
        <li id="educationTourPage" class="{{active('education_tours.*')}}">
          <a href="/education_tours">
            <i class="fa fa-tree"></i>
            <span>Education Tour</span>
          </a>
        </li>
        {{--  <li id="bundlingPage" class="{{active('bundlings.*')}}">
          <a href="/bundlings">
            <i class="fa fa-files-o"></i>
            <span>Bundling</span>
          </a>
        </li>  --}}
        <li id="facilityPage" class="{{active('facilities.*')}}">
          <a href="/facilities">
            <i class="fa fa-building"></i>
            <span>Facility</span>
          </a>
        </li>
        <li id="addonPage" class="{{active('addons.*')}}">
          <a href="/addons">
            <i class="fa fa-plus"></i>
            <span>Addon</span>
          </a>
        </li>
        <li id="Bundle" class="{{active('bundles.*')}}">
          <a href="{{ route('bundles.index') }}">
            <i class="fa fa-star"></i>
            <span>Paket</span>
          </a>
        </li>
        @endif
        @if(Auth::user()->role<=2)
    <li class="treeview {{active('orders.*')}} {{active('coupons.*')}}">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Transaction</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li class="{{active('orders.*')}}">
              <a href="{{ route('orders.index') }}" >
                <i class="fa fa-cart-plus"></i>
                <span>Order</span>
              </a>
            </li>
        @if(Auth::user()->role==1)                    
            <li class="{{active('coupons.*')}}">
              <a href="{{ route('coupons.index') }}" >
                <i class="fa fa-ticket"></i>
                <span>Coupons</span>
              </a>
            </li>
          @endif
          </ul>
        </li>
        @endif
        
        @if(Auth::user()->role==1)        
        <li class="{{active('reports.*')}}">
          <a href="{{ route('reports.index') }}" >
            <i class="fa fa-book"></i>
            <span>Laporan</span>
          </a>
        </li>
        
        <li  id="fasilitasPage" class="treeview  {{active('custom_prices.*')}}{{active('customer_types.*')}}{{active('units.*')}}{{active('users.*')}}{{active('galleries.*')}} {{active('facility_attributes.*')}} {{active('facility_details.*')}}{{active('facility_types.*')}}">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Pengaturan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu">
            <li id="attributePage" class="{{active('facility_attributes.*')}}">
              <a href="/facility_attributes">
                <i class="fa fa-cube"></i>
                <span>Facility Attribute</span>
              </a>
            </li>
            <li id="unit" class="{{active('units.*')}}">
              <a href="/units">
                <i class="fa fa-cube"></i>
                <span>Unit</span>
              </a>
            </li>
<!--             <li id="facilityDetailPage" class="{{active('facility_details.*')}}">
              <a href="/facility_details">
                <i class="fa fa-cube"></i>
                <span>Facility Detail</span>
              </a>
            </li> -->
            <li id="typePage" class="{{active('facility_types.*')}}">
              <a href="/facility_types">
                <i class="fa fa-cube"></i>
                <span>Facility Type</span>
              </a>
            </li>
            <li id="typePage" class="{{active('custom_prices.*')}}">
              <a href="/custom_prices">
                <i class="fa fa-cube"></i>
                <span>Custom Price</span>
              </a>
            </li>
            {{--  <li id="typePage" class="{{active('special_prices.*')}}">
              <a href="/special_prices">
                <i class="fa fa-cube"></i>
                <span>Special Price</span>
              </a>
            </li>  --}}
            <li id="typePage" class="{{active('customer_types.*')}}">
              <a href="/customer_types">
                <i class="fa fa-cube"></i>
                <span>Customer Type</span>
              </a>
            </li>
            <li id="galleryPage" class="{{active('galleries.*')}}">
              <a href="/galleries">
                <i class="fa fa-image"></i>
                <span>Gallery</span>
              </a>
            </li>
            <li id="userPage" class="{{active('users.*')}}">
              <a href="/users" >
                <i class="fa fa-user"></i>
                <span>User</span>
              </a>
            </li>
          </ul>
        </li>
        @endif
        <li>
          <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out"></i>
            <span>Logout</span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>