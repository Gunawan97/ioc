<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!-- Required meta tags -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <style>

    table * {
      font-size: 13px;
      width:340px;
    }
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:20px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }
    table#result { border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{ border: solid thin; }
    table#det td{ border: solid thin; }

  </style>
</head>
<body>
{{--  <div class="col-xs-4" align="center">
    <img  width="50%" src="{{ asset('image/Logo_Warna_Berdiri_400x400.jpg')}}"/>    
</div>
<div class="col-xs-4">  --}}
<h4 align="center">Integrated Outdoor Campus - Trawas</h2>
<h3 align="center" style="font-family:serif;">Ubaya Training Center</h2>
<h5 align="center">Laporan Order Berdasarkan Fasilitas</h3>
<h6 align="center">{{ date("F")." ".date("Y") }}</h3>
{{--  </div>
<div class="col-xs-4" align="center" >
    <img width="70%" src="{{ asset('image/logoutc.png')}}"/>
</div>  --}}
<br>
@php($i=0)
<section class="page">
@foreach($result as $key=>$res)
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <table>
    <tr><td>Jenis Fasilitas</td>
    <td>:</td>
    <td>{{$key}}</td>
    </tr>
    <tr><td>TANGGAL</td>
    <td>:</td>
    <td>{{ $dated }}</td>
    </tr>
    </table>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>No. Order</th>
        <th>Nama Customer</th>
        <th>Tanggal Order</th>
        <th>Tanggal Check-in</th>
        <th>Tanggal Check-out</th>
        <th>Fasilitas</th>
        <th>Total</th>
      </tr>
      @php($tot=0)
      @foreach($res as $key => $ord)
      <tr>
          <td>{{$key+1}}</td>
          <td>{{$ord->order_code}}</td>
          <td>{{ $ord->cname }}</td>
          <td>{{$ord->date}}</td>
          <td>{{$ord->check_in_label->format("Y-m-d")}}</td>
          <td>{{$ord->check_out_label->format("Y-m-d")}}</td>
          <td>{{$ord->nameLabel}}</td>
          <td>{{H::rupiah($ord->totalLabel)}}</td>
          @php($tot+=$ord->totalLabel)
      </tr>
      @endforeach
      <tr>
          <td colspan="7" align="center"><b>Total<b></td>
          <td><b>{{ H::rupiah($tot) }}<b></td>
      </tr>
  </table>
</div>

</div>
</div>

{{--  ==================================================================  --}}

@php($i++)
 @endforeach
</section>
 


@foreach($rest as $key=>$res)
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <table>
    <tr><td>Jenis</td>
    <td>:</td>
    <td>{{$key}}</td>
    </tr>
    <tr><td>TANGGAL</td>
    <td>:</td>
    <td>{{ $dated }}</td>
    </tr>
    </table>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>No. Order</th>
        <th>Nama Customer</th>
        <th>Tanggal Order</th>
        <th>Item</th>
        <th>Total</th>
      </tr>
      @php($tot=0)
      @foreach($res as $key => $ord)
      <tr>
          <td>{{$key+1}}</td>
          <td>{{$ord->order_code}}</td>
          <td>{{ $ord->cname }}</td>
          <td>{{$ord->date}}</td>
          <td>{{$ord->nameLabel}}</td>
          <td>{{H::rupiah($ord->totalLabel)}}</td>
          @php($tot+=$ord->totalLabel)
      </tr>
      @endforeach
      <tr>
          <td colspan="5" align="center"><b>Total<b></td>
          <td><b>{{ H::rupiah($tot) }}<b></td>
      </tr>
  </table>
</div>

</div>
</div>
</section>

{{--  ==================================================================  --}}

@php($i++)
 @endforeach
</body>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!--<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/uxs/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> -->
   
</html>
