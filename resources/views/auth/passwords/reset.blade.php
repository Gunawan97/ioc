@extends('layouts.guest')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <div class="panel-heading">Reset Password</div>
    </div>

    <div class="login-box-body">
        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
              <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">                                   
         <input id="password" placeholder="Password" type="password" class="form-control" name="password" required>
         @if ($errors->has('password'))
         <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">                                   
         <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
         @if ($errors->has('password_confirmation'))
         <span class="help-block">
          <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
        @endif
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>


        <div class="form-group">
                <center>
                <button type="submit" class="btn btn-primary">
                    Reset Password
                </button>
                </center>
        </div>
    </form>
</div>
</div>
@endsection


