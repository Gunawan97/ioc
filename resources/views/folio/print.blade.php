<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!-- Required meta tags -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <style>

    table * {
      font-size: 13px;
      width:340px;
    }
    table th{
      text-transform: uppercase;
      text-align: center;
      
    }
    td{
      text-align: center;
    }

    .kop-surat p{
      font-size: 1.2em;
    }
    table thead { display: table-header-group; }
    table tr { page-break-inside: avoid; }
    section.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .row{
      padding:20px;
    }
    .holiday{
      background-color: grey;
      color:white;
    }
    table#result { border-collapse: collapse; }
    table#result tr { border: solid thin; }
    table#det th{ border: solid thin; }
    table#det td{ border: solid thin; }

  </style>
</head>
<body>

<h4 align="center">Integrated Outdoor Campus - Trawas</h2>
<h3 align="center" style="font-family:serif;">Ubaya Training Center</h2>
<h5 align="center">Laporan Order Berdasarkan Pelanggan</h3>
<h6 align="center">{{ date("F")." ".date("Y") }}</h3>

<br>

@php($i=0)
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <h3>List Fasilitas</h3>
    <h5>{{ $dated }}</h5>
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>Item</th>
        <th>Tipe</th>
        <th>Tanggal Check-in</th>
        <th>Tanggal Check-out</th>
        {{--  <th>Total</th>  --}}
      </tr>
      @php($tot=0)
      @foreach($facs as $key => $folio)
      <tr>
        <td>{{ $key+1 }}</td>          
        <td> {{ $folio->nameLabel }} </td>
        <td>{{ $folio->typeLabel }}</td>
        <td>{{ $folio->check_in->format("d-m-Y") }}</td>
        <td>{{ $folio->check_out->format("d-m-Y") }}</td>
      </tr>
      @endforeach
   
  </table>
</div>

</div>
</div>
</section>

{{--  ==================================================================  --}}
<section class="page">
<div class="row">
<div class="col-xs-12">
<div class="col-xs-6">
    <h4>List Tambahan</h4>
    <h5>Tanggal: {{ $dated }}</h5>
    
</div>
<div class="col-xs-12">
  <table id="det">
      <tr>
        <th>No.</th>
        <th>Item</th>
        
        <th>Jumlah</th>
        <th>Tanggal Pesan</th>
      </tr>
      @php($tot=0)
      @foreach($addons as $key => $folio)
      <tr>
        <td>{{ $key+1 }}</td>          
        <td> {{ $folio->nameLabel }} </td>
        {{--  <td>{{ $folio->typeLabel }}</td>  --}}
        <td>{{ $folio->amount }}</td>
        <td>{{ $folio->check_in->format("d-m-Y") }}</td>
      </tr>
      @endforeach
  </table>
</div>

</div>
</div>
</section>

</body>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!--<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/uxs/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> -->
   
</html>
