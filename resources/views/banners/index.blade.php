@extends('layouts.app')

@section('title')
Manage Banner
@endsection

@section('content')

<div class="row">
	<!-- Search User -->
	<form method="get" id="search-form">
		<div class="col-md-3">
			<div class="input-group no-border">
				<input type="text" class="form-control column-filter" name="filter[title]" placeholder="Title" value="{{ !empty($filter['title']) ? $filter['title'] : '' }}"/>
				<span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
			</div>
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-primary btn-block">Cari</button>
		</div>
	</form>
</div>
<div class="box">
	<div class="box-header">
		<div class="row">
			<div class="col-md-3">
				<a href="{{ route('banners.create')}}" class="btn btn-success btn-block">Tambah Banner &nbsp;<span class="fa fa-plus-circle"></span></a>
			</div>
		</div>
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Image (Desktop)</th>
                    <th>Image (Mobile)</th>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th>Description</th>
					<th>Link Text</th>
					<th>Link URL</th>
					<th>Banner Location</th>
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				@foreach($banner as $b)
				<tr>
					<td>{{$b->id}}</td>
					<td>{{$b->image_desktop_path}}</td>
                    <td>{{$b->image_mobile_path}}</td>
					<td>{{$b->title}}</td>
					<td>{{$b->subtitle}}</td>
                    <td>{{$b->description}}</td>
                    <td>{{$b->link_text}}</td>
                    <td>{{$b->link_url}}</td>
                    <td>{{$b->banner_location_id}}</td>
					<td>
						<a href="{{route('banners.edit',$b->id)}}">
							<button class="btn btn-warning">
								Edit
							</button>
						</a>
					</td>
				</tr>
				@endforeach
				@if($banner->count() == 0)
					<tr>
					<td colspan="7"><center><em>Tidak Ada Data.</em></center></td>
					</tr>
				@endif
			</tbody>
		</table>
		@php
		$filters = [];
		foreach ($filter as $key => $value) {
		$filters['filter['. $key .']'] = $value;
	}
	@endphp
	{{ $banner->appends($filters)->links() }}
</div>
</div>

@stop
