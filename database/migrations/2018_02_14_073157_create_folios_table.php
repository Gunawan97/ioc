<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'folios', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('folio_id');
                $table->string('folio_type');
                // $table->integer('price');
                $table->integer('detail_order_id')->unsigned();
                $table->timestamp('check_in')->nullable();
                $table->timestamp('check_out')->nullable();
                // $table->timestamp('actual_in')->nullable();
                // $table->timestamp('actual_out')->nullable();
                $table->integer('amount');
                $table->integer('active')->default(1);
                $table->timestamps();

                $table->foreign('detail_order_id')->references('id')->on('detail_orders');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folios');
    }
}
