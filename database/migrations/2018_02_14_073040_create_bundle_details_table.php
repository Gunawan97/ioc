<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'bundle_details', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('bundl_id');
                $table->string('bundl_type');
                $table->integer('bundle_id')->unsigned();
                $table->timestamps();

                $table->foreign('bundle_id')->references('id')->on('bundles');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_details');
    }
}
