<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponFacilityTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'coupon_facility_types', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('facility_type_id')->unsigned();
                $table->integer('coupon_id')->unsigned();
                $table->integer('exclude')->default(0);
                $table->timestamps();

                $table->foreign('facility_type_id')->references('id')->on('facility_types');
                $table->foreign('coupon_id')->references('id')->on('coupons');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_facility_types');
    }
}
