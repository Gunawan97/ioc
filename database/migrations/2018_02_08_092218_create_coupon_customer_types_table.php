<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCustomerTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'coupon_customer_types', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('customer_type_id')->unsigned();
                $table->integer('coupon_id')->unsigned();
                $table->integer('exclude')->default(0);
                $table->timestamps();
                $table->foreign('customer_type_id')->references('id')->on('customer_types');
                $table->foreign('coupon_id')->references('id')->on('coupons');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_customer_types');
    }
}
