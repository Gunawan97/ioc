<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'orders', function (Blueprint $table) {
                $table->increments('id');
                $table->string('order_code')->nullable();
                $table->integer('status')->default(0);
                $table->integer('customer_id')->unsigned();
                $table->date('date');
                // $table->date('check_out');
                $table->integer('adult')->default(1);
                $table->integer('children')->default(0);
                $table->string('description')->nullable();
                $table->string('code')->nullable();
                $table->integer('disc')->nullable();
                $table->integer('tax')->nullable();
                $table->integer('coupon_id')->unsigned()->nullable();
                $table->integer('lunas')->default(0);
                $table->integer('globaldisc_type')->default(0);
                $table->integer('globaldisc')->default(0);
                $table->integer('menginap')->default(1);
                $table->integer('expiry')->default(3);
                $table->timestamps();

                $table->foreign('coupon_id')->references('id')->on('coupons');
                $table->foreign('customer_id')->references('id')->on('customers');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
