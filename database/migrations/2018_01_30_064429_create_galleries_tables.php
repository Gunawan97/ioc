<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create(
            'galleries', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('galleriable_id')->unsigned()->nullable();
                $table->string('galleriable_type');
                $table->string('image_path');
                $table->string('slug')->unique();
                $table->string('title');
                $table->string('description');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'galleries', function (Blueprint $table) {
                //
            }
        );
    }
}
