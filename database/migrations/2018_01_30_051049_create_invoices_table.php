<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'invoices', function (Blueprint $table) {
                $table->increments('id');
                $table->string('invoice_code')->nullable();
                $table->timestamp('date');
                $table->integer('order_id')->unsigned();
                $table->timestamps(); 
                $table->foreign('order_id')->references('id')->on('orders');

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
