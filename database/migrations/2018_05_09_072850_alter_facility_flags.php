<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilityFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('facilities', function($table) {
            $table->integer('front_end')->default(1);
            $table->integer('back_end')->default(1);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {  
        Schema::table('facilities', function($table) {
            $table->integer('front_end')->default(1);
            $table->integer('back_end')->default(1);
        });
    }
}
