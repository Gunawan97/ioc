<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'detail_orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order_id')->unsigned();
                $table->integer('item_id');
                $table->string('item_type');
                $table->integer('amount')->nullable();
                $table->integer('amount2')->nullable(0);
                $table->integer('disc')->nullable();
                // $table->integer('tax')->nullable();
                $table->integer('price');
                $table->integer('status')->default(0);
                $table->integer('subtotal')->default(0);
                $table->text('note')->nullable();
                $table->timestamps();

                $table->foreign('order_id')->references('id')->on('orders');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_orders');
    }
}
