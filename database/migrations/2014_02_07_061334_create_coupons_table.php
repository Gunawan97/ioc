<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'coupons', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code');
                $table->string('description')->nullable();
                $table->integer('type');
                $table->integer('amount');
                $table->date('expiry');
                $table->date('start');
                $table->integer('max_spend')->nullable();
                $table->integer('min_spend')->nullable();
                $table->integer('indiv_use')->default(0);
                $table->integer('exclude_sale')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
