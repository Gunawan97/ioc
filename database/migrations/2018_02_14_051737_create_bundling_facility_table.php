<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundlingFacilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'bundling_facility', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('bundling_id');
                $table->foreign('bundling_id')->references('id')->on('bundlings');
                $table->integer('bundlingable_id')->unsigned();
                $table->string('bundlingable_type');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundling_facility');
    }
}
