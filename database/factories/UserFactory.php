<?php

// use Faker\Generator as Faker;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $factory->define(App\User::class, function (Faker $faker) {
//     static $password;

//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),
//     ];
// });

function autoIncrement()
{
    for ($i = 0; $i < 99999; $i++) {
        yield $i;
    }
}

$autoIncrement = autoIncrement();

$factory->define(
    App\Customer::class, function (Faker\Generator $faker) use ($autoIncrement) {
    
        $autoIncrement->next();
    
        return [
        'name' => $faker->name,
        'email' => $faker->email,
        'city_id' => 1,
        'zip' => rand(6200, 6299),
        'card_id' => str_pad(rand(1, 999).$autoIncrement->current(), 5, '0', STR_PAD_RIGHT),
        'address' => $faker->address,
        'customer_type_id' => rand(1, 2),
        'phone' => $faker->phoneNumber,
        ];
    }
);


$factory->define(
    App\Facility::class, function (Faker\Generator $faker) use ($autoIncrement) {
        $sets = [1=>'Room',2=>'Dorm'];
        $nm = $faker->lastName;
        $type = [1=>'Deluxe',2=>'King',3=>'Queen', 4=>'Twin',5=>'Suprem', 6=>'Jack', 7=>'Premium'];
        $na = rand(1, 2);
        $name = $sets[$na]." ".$type[rand(1, 7)]." ".$nm;
        $autoIncrement->next();
        $cur=$autoIncrement->current();

        $pers =true;
        if($na==2) {
            $pers=false;
        }
        return [
        'name' => $name,
        'slug' => $cur."-".$name,
        'price' => rand(0, 900)."000",
        'capacity' => rand(1, 20) ,
        'type_id' => $na,
        'per_person' => $pers,
        'unit' => $na,
        
        'description' => "<p> no desc</p>",
        ];
    }
);

$factory->define(
    App\FacilityDetail::class, function (Faker\Generator $faker) use ($autoIncrement) {
        $attr=rand(1, 2);
        if($attr==1) {
            $val = rand(1, 5);
        }
        else{
            $val = rand(1, 2);
        }
        return [
        'facility_id' => rand(1, 50),
        'attribute_id' => $attr,
        'value' => $val ,
        ];
    }
);

$factory->define(
    App\DetailFacility::class, function (Faker\Generator $faker) use ($autoIncrement) {
        $name = rand(1, 9).rand(1, 9).rand(1, 9);
        $autoIncrement->next();
   
        return [
        'name' => $name,
        'facility_id' => rand(1, 50),
        ];
    }
);

